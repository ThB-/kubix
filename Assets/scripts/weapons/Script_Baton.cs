﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Script_Baton : MonoBehaviour
{
    private GameObject baton;
    void Start()
    {
        baton = GameObject.Find("Wand_2");
        baton.SetActive(false);
    }

    public void activerBaton()
    {
        baton.SetActive(true);
        Debug.Log("je passe là!");
    }

    public void desactiverBaton()
    {
        baton.SetActive(false);
    }



}
