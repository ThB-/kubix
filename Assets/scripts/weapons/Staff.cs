﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Staff : MonoBehaviour
{
    private Transform position;
    float x, y, z;
    Vector3 vecteur;
    public GameObject txteBaton;
    bool dehors = false;
    private GameObject cloneTexte;
    private Script_Baton leBaton;


    // Start is called before the first frame update
    void Awake()
    {
        position = GetComponent<Transform>();
        x = position.position.x;
        y = 3.5f;
        z = position.position.z;
        vecteur = new Vector3(x,y,z);


    }


    void OnTriggerEnter(Collider autre)
    {
        var cloneTexte = Instantiate(txteBaton, vecteur, Quaternion.Euler(0, 45, 0)) as GameObject;
        Destroy(cloneTexte, 5);
    }

    void OnTriggerExit(Collider autre)
    {
        dehors = true;
    }



}
