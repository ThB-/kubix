﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lancer_sort : MonoBehaviour
{
    public GameObject sort_clicGauche;
    public AudioSource son_a_jouer;
    private Vector3 posSouris;
    private GameObject baton_bois;
    public GameObject cam;
    private Gestion_Cam gerer_cam;
    private Vector3 vec2;
    public GameObject player;
    private Transform transf;
    public float temps = 5.0f;
    private Vector3 vec;

    public float fireRate = 0.5f;
    private float nextFire = 0.0f;

    void Start()
    {
        baton_bois = GameObject.Find("/Player/Body/ArmL/HandL/_holder/Wand_2");
        gerer_cam = cam.GetComponent<Gestion_Cam>();
        transf = player.GetComponent<Transform>();
        vec = new Vector3(0.0f,1.0f,2.0f);


    }

    // Update is called once per frame
    void Update()
    {


        if (baton_bois.activeSelf)
        {
            vec = (Input.mousePosition);
            vec.z = 10.0f;
            var position = Camera.main.ScreenToWorldPoint(vec);

            if (Input.GetKeyDown(KeyCode.Mouse0))
            {

                if (Time.time > nextFire)
                {
                    nextFire = Time.time + fireRate;
                    son_a_jouer.Play();

                    var sort = Instantiate(sort_clicGauche, position, Quaternion.identity);

                    Destroy(sort, 2);
                }

            }


        }

    }
}
