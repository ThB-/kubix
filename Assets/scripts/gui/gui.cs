﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gui : MonoBehaviour
{
    public float vitesse = 5.0f;
    private Transform t;
    void Start()
    {
        t = GetComponent<Transform>();
    }
    void Update()
    {
        t.position += Vector3.up * vitesse * Time.deltaTime; // Le texte défile et se précipite vers le haut de l'écran
    }
}
