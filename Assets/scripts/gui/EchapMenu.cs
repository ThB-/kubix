﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EchapMenu : MonoBehaviour
{
    private bool pause = false;


    void update()
    {

        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("oui!");
            pause = true;
        }
    }

    void OnGUI()
    {
        if (pause)
        {
            Debug.Log("Je passe ici!");
            GUILayout.Label("Jeu en pause!");
            if (GUILayout.Button("Cliquez pour retier la pause"))
                pause = false;
        }
    }

}
