﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    private bool pause = false;

    Rect windowRect = new Rect(750, 400, 500, 200);


    void Start()
    {
    }
    void OnGUI() //Gestion affichage
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            pause = true;
  
        }
        if (pause)
        {
            Time.timeScale = 0.0f; // Arrêt du temps
            windowRect = GUILayout.Window(0, windowRect, menuPause, "Menu Pause");
        }
        else
        {
            Time.timeScale = 1.0f; //Le jeu reprend sa viteses normale
        }
    }

    void menuPause(int windowID)
    {

        if (GUILayout.Button("Reprendre",GUILayout.Height(100)))
        {
            pause = false;
        }
        if (GUILayout.Button("Quitter", GUILayout.Height(100)))
        {
            Application.Quit(); // Arrêt du jeu
        }
    }

}
