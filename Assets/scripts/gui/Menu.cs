﻿ using UnityEngine;
 using UnityEngine.UI;
 using System.Collections;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
 {
     public Button CustomButton;
 
     void Awake()
     {
         Button btn = CustomButton.GetComponent<Button>();
         btn.onClick.AddListener(CustomButton_onClick); // Je veux agir avec le bouton
     }
 
     void CustomButton_onClick() //Attend d'être cliqué
     {
        Loadscene(); 
     }

    void Loadscene()
    {
        SceneManager.LoadScene("hub"); // Chargement d'une zone
    }
 }