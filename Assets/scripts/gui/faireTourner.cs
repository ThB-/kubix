﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class faireTourner : MonoBehaviour
{
    public float vitesse = 2.0f;
    private RectTransform rectangle;
    private float x = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        rectangle = GetComponent<RectTransform>(); //Position du texte
    }

    // Update is called once per frame
    void Update()
    {
        x = (x + 1)%360 ;
        rectangle.rotation = Quaternion.Euler(0, x ,0); // Le texte tourne sur l'angle Y % 360
    }
}
