﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Button_Quit : MonoBehaviour
{
    public Button bouton;
    void Start()
    {
        bouton.onClick.AddListener(action_quitter);

    }

    void action_quitter()
    {
        Application.Quit();
    }
}
