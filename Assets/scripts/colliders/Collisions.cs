﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour
{
    private Transform t;
    private Transform posBaton;
    public GameObject textCow;
    public GameObject textBaton;
    public GameObject baton;
    float x, y, z;
    Vector3 position;
    // Le texte qui défilera au-dessus la vache
    // Gérer les collisions

    void Awake()
    {
        t = GetComponent<Transform>(); // Initialisaton position joueur
        posBaton = baton.GetComponent<Transform>();



    }

    void Start()
    {
        float x = posBaton.position.x;
        float y = 3.0f;
        float z = posBaton.position.z;

        position = new Vector3(x,y,z);

    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Cow_1") // Collisions avec la vache
        {

            if (Input.GetKey(KeyCode.Mouse0))
            {
                Instantiate(textCow, t.position, Quaternion.Euler(0, 45, 0));
                // Le texte va spawn au-dessus du joueur avec un angle Y = 45
            }

        }
    }
}

