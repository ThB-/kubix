﻿using UnityEngine;
using System.Collections;

public class IA  : MonoBehaviour
{

    public Transform laCible;

    private Transform monstre_mouvement;

    public float speed = 0.50f;
    private Animator animations;

    private int alea = 0;
    private float distance;
    private bool enrage = false;


    public float fireRate = 1.0f;
    private float nextFire = 0.0f;

    public GameObject spell_ennemi;

    private Vector3 vec;


    private bool disponible = true;



    void Awake()
    {

        monstre_mouvement = GetComponent<Transform>();
        animations = GetComponent<Animator>();
        distance = 2.0f;
        vec = new Vector3(0.0f, 0.0f, 2.0f);
    }

    void Start()
    {
        animations.SetBool("OnGround",true);
        animations.SetFloat("Speed", 5.0f);
    }

    void Update()
    {
        if (disponible)
        {
            StartCoroutine(IndiquerDir());
        }

       // if (enrage)
        //{
          //  monstre_mouvement.LookAt(laCible,Vector3.forward);
          //  Debug.Log("je passe ici!");
       // }
        //else
       // {
            if (alea == 1)
            {
                monstre_mouvement.position += Vector3.forward * speed * Time.deltaTime;
                monstre_mouvement.rotation = Quaternion.Euler(0, 180, 0);

            }
            if (alea == 2)
            {
                monstre_mouvement.position += Vector3.left * speed * Time.deltaTime;
                monstre_mouvement.rotation = Quaternion.Euler(0, 90, 0);

            }

            if (alea == 3)
            {
                monstre_mouvement.position += Vector3.right * speed * Time.deltaTime;
                monstre_mouvement.rotation = Quaternion.Euler(0, 270, 0);
            }

            if (alea == 4)
            {
                monstre_mouvement.position += -Vector3.forward * speed * Time.deltaTime;
                monstre_mouvement.rotation = Quaternion.Euler(0, 0, 0);
            }

            if(Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            var trucdeFou = Instantiate(spell_ennemi,monstre_mouvement.position + vec,Quaternion.identity);
            Destroy(trucdeFou, 10);
        }
        //}


       // if(monstre_mouvement.position.x - laCible.position.x <   distance)
        //{
          //  enrage = true;
       // }


    }

    IEnumerator IndiquerDir()
    {
        disponible = false;
        alea = Random.Range(1, 4);
        yield return new WaitForSeconds(1);
        disponible = true;

    }

  





}