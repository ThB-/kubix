﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Vie_Joueur : MonoBehaviour
{
    public float pts_vie;
    public GameObject Player;
    public GameObject explo_sang;
    private bool subit_degats = false;
    private bool fini = true;

    void Update()
    {
        if(pts_vie < 1)
        {
            Destroy(Player);
            SceneManager.LoadScene("Ecran_Game_Over");
        }
        
    }



    void OnTriggerEnter(Collider autre)
    {
        if(autre.gameObject.tag == "Attaque_Boss")
        {
            if (fini)
            {

                this.pts_vie -= 1;
                StartCoroutine(Subir_Degats());
                fini = false;
            }
           
        }
    }

    IEnumerator Subir_Degats()
    {
        Instantiate(explo_sang, Player.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Instantiate(explo_sang, Player.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        Instantiate(explo_sang, Player.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(1);
        fini = true;
    }
}
