﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class boss : MonoBehaviour
{
    public GameObject dragon;
    public GameObject attaque;
    private bool deplacer_droite,deplacer_gauche,Attaquer = false;
    public float temps = 5.0f;
    private float nb1;
    private bool fini = true;
    private Vector3 position_cracher_feu;


    void Start()
    {
    }


    void Update()
    {
        if (fini)
        {
            StartCoroutine(Exemple());
            fini = false;
        }
        if (nb1 == 2.0f)
        {
            dragon.transform.position += Vector3.right * temps * Time.deltaTime;
        }
        if (nb1 == 3.0f)
        {
            dragon.transform.position += -Vector3.right * temps * Time.deltaTime;
        }
        if (nb1 == 5.0f)
        {
            StartCoroutine(Attente_cra_feu());
            dragon.transform.Translate(Vector3.right * temps * Time.deltaTime);
        }


    }

    IEnumerator Exemple()
    {
        nb1 = 2.0f;
        yield return new WaitForSeconds(3);
        nb1 = 3.0f;
        yield return new WaitForSeconds(3);
        nb1 = 5.0f;
        yield return new WaitForSeconds(1);
        fini = true;
    }

    IEnumerator Attente_cra_feu()
    {
        cracher_feu();
        yield return new WaitForSeconds(1);
    }


    void cracher_feu()
    {

        position_cracher_feu.x = dragon.transform.position.x;
        position_cracher_feu.y = dragon.transform.position.y;
        position_cracher_feu.z = dragon.transform.position.z - 2.0f;
        var cra_feu = Instantiate(attaque, position_cracher_feu,Quaternion.Euler(0,180,0));
        Destroy(cra_feu, 1);
    }
}