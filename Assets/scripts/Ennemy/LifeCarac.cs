﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeCarac : MonoBehaviour
{
    public AudioSource son_a_jouer;
    public float points_de_vie;
    public GameObject perso;
    private Vector3 pos;


    void Awake()
    {
        points_de_vie = 3.0f;

    }
    void Update()
    {
        if (points_de_vie == 0)
        {
            Destroy(perso);
        }

    }

    void OnCollisionEnter(Collision autre)
    {

        if (autre.gameObject.tag == "Attaques")
        {
            son_a_jouer.Play();
            Debug.Log("OUIIIIIIIIII");
            points_de_vie -= 1;
        }

    }

}
