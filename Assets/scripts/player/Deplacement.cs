﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Deplacement : MonoBehaviour
{

    // Initialisation de toutes les variables nécessaires
    private Transform t; // Position joueur
    public float vitesse = 5.0f; 
    public float runVitesse = 2 * 5.0f;
    public Animator anim; // Animations
    private bool toucheZ, toucheS, toucheQ, toucheD,attaquer = false;
    public Texture2D textureToDisplay;
    public Slider gui; // Barre d'endurance
    private GameObject theStaff;
    void Start()
    {
        t = GetComponent<Transform>(); // Positions
        anim = GetComponent<Animator>(); // Animations
        theStaff = GameObject.Find("/Player/Body/ArmL/HandL/_holder/Wand_2");
    }

    void FixedUpdate()
    {
        // Mode Idle
        anim.SetBool("OnGround",true);
        anim.SetFloat("Speed",0.0f);

        // Mode animation courrir
        if (bouger())
        { 
            anim.SetFloat("Speed", 5.0f);
        }
        else
        {
            anim.SetFloat("Speed", 0.0f);
        }

        // Les mouvements :


        if (Input.GetKey(KeyCode.R)) // Boost de vitesse lors de l'appui sur R
        {
            if (gui.value != 0.0f) // Representé sous forme de slider
            {
                vitesse = runVitesse;
                gui.value -= 0.01f;

            }
            else
            {
                vitesse = 2.0f;
            }
        }
        else
        {
            vitesse = 5.0f;
        }

        
        // Avancer
        if (Input.GetKey(KeyCode.Z))
        {
            t.position += Vector3.forward * vitesse * Time.deltaTime;
            t.rotation = Quaternion.Euler(0, 180, 0);
            toucheZ = true;

        }
        else
        {
            toucheZ = false;
        }


        // Reculer
        if (Input.GetKey(KeyCode.S))
        {
            //Move the Rigidbody upwards constantly at speed you define (the green arrow axis in Scene view)
            t.position += -Vector3.forward * vitesse * Time.deltaTime;
            t.rotation = Quaternion.Euler(0, 0, 0);
            toucheS = true;
        }
        else
        {
            toucheS = false;
        }


        // Aller à gauche
        if (Input.GetKey(KeyCode.Q))
        {
            //Move the Rigidbody upwards constantly at speed you define (the green arrow axis in Scene view)
            t.position += -Vector3.right * vitesse * Time.deltaTime;
            t.rotation = Quaternion.Euler(0, 90, 0);
            toucheQ = true;
        }
        else
        {
            toucheQ = false;
        }


        // Aller à droite
        if (Input.GetKey(KeyCode.D))
        {
            //Move the Rigidbody upwards constantly at speed you define (the green arrow axis in Scene view)
            t.position += Vector3.right * vitesse * Time.deltaTime;
            t.rotation = Quaternion.Euler(0, 270, 0);
            toucheD = true;
        }
        else
        {
            toucheD = false;
        }

        //Sauter
        if (Input.GetKey(KeyCode.Space))
        {
            //Move the Rigidbody upwards constantly at speed you define (the green arrow axis in Scene view)
            t.position += Vector3.up * vitesse * Time.deltaTime;
            anim.SetBool("OnGround", false);

        }
        else
        {
            anim.SetBool("OnGround", true);
        }


        // Gestion de l'orientation personnage suivant l'angle

        if (toucheZ && toucheD)
        {
            t.rotation = Quaternion.Euler(0,225,0);
        }

        if (toucheZ && toucheQ)
        {
            t.rotation = Quaternion.Euler(0,140,0);
        }

        if (toucheS && toucheD)
        {

            t.rotation = Quaternion.Euler(0,315,0);
        }

        if (toucheS && toucheQ)
        {

            t.rotation = Quaternion.Euler(0, 45, 0);
        }


        // Est-ce que j'attaque ?
        if (Input.GetKey(KeyCode.Mouse0))
        {
            attaquer = true;
        }
        else
        {
            attaquer = false;
        }

        // Animation attaquer
        if (attaquer)
        {
            anim.SetBool("Attack1",true);
        }
        else
        {
            anim.SetBool("Attack1", false);
        }

    }

    // Est-ce que je bouge actuellement ?
    public bool bouger()
    {
        if (toucheZ || toucheS || toucheQ || toucheD)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


}
