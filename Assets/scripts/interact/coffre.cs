﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coffre : MonoBehaviour
{
    public GameObject arme_a_loot;
    private GameObject player;
    public AudioSource son_loot;
    private bool dejaOuvert = false;
    void Start()
    {
        

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerStay(Collider autre)
    {
        if(autre.gameObject.tag == "Player")
        {
            if (Input.GetKey(KeyCode.E) && !dejaOuvert)
            {
                son_loot.Play();
                Instantiate(arme_a_loot, player.transform.position, Quaternion.Euler(0, 90, 0));
                dejaOuvert = true;
            }
        }
    }
}
