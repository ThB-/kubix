﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gestion_Cam : MonoBehaviour
{
    private Vector3 offset;
    public GameObject Player;
    public AudioSource leson;
    void Start()
    {
        leson.volume = 0.1f;
        leson.Play();
        offset = (transform.position - Player.transform.position); // La caméra garde une distance avec le joueur
    }

    void Update()
    {
        transform.position = Player.transform.position + offset;
        //La caméra suit le joueur
    }

    public Vector3 getOffset()
    {
        return offset;
    }

    public float getOffsetZ()
    {
        return offset.z;
    }


}
