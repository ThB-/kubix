﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class faire_tourner_cube : MonoBehaviour
{

    public float temps = 5.0f;
    private Transform transform;
    private float Angle = 60.0f;
    void Start()
    {
        transform = GetComponent<Transform>();
    }

    void Update()
    {
        transform.Rotate(Vector3.up * (temps* Time.deltaTime));
        transform.Rotate(Vector3.left * (temps * Time.deltaTime));
    }
}
